# How to use
0. You must have python3 installed first
1. Download zip file
2. Unzip zip file
3. Open terminal (on mac hit command + spacebar and type in terminal)
4. Type `cd Downloads/electron_lines-master`
5. Type `bash install.sh`
6. Wait for script to finish installing dependencies
7. Once finished, type `source myvenv/bin/activate`
8. Type `python3 plot.py` to open graph of charges
