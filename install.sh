#!./myvenv/bin/python

python3 -m pip install --user --upgrade pip
python3 -m pip install virtualenv
python3 -m venv myvenv
activate() {
	. $PWD/myvenv/bin/activate
}
source myvenv/bin/activate
python3 -m pip install -r requirements.txt
activate
